#include <stdio.h>
#include <stdlib.h>


int main(void) {
    int array[10];
    int arr[10] = {};

    printf("array[10] = ");
    for (int i = 0; i < 10; ++i) {
        printf(" %d", array[i]);
    }
    putchar('\n');

    printf("arr[10] = ");
    for (int i = 0; i < 10; ++i) {
        printf(" %d", arr[i]);
    }
    putchar('\n');

    exit(EXIT_SUCCESS);
}
