# Initializing variables
prog_path=c_programming_a_modern_approach/ch$1/pp$2
c_file=$prog_path/pp$2.c
bin_dir_path=$prog_path/bin
bin_path=$bin_dir_path/pp$2

# Check to see if the bin directory exists
# If not, create the directory
if [ ! -d $bin_dir_path ];
then
    mkdir $bin_dir_path
fi

# Compile the C source code and dump the binary
gcc -o $bin_path $c_file

# Execute the binary
$bin_path
